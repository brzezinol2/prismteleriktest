﻿using System.Collections.Generic;
using System.Linq;
using Telerik.XamarinForms.Common;

namespace PrismTelerikTest.ViewModels
{
    public class TreeViewDemoAdapter : IHierarchyAdapter
    {
        public object GetItemAt(object item, int index)
        {
            return (item as Item).Children[index];
        }

        public IEnumerable<object> GetItems(object item)
        {
            return (item as Item).Children ?? Enumerable.Empty<object>();
        }
    }
}
