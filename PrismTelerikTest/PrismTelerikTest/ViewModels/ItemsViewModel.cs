﻿using Prism.Mvvm;
using System.Collections.ObjectModel;

namespace PrismTelerikTest.ViewModels
{
    public class ItemsViewModel : BindableBase
    {
        public ItemsViewModel()
        {
            SourceCollection = new ObservableCollection<Item>();
        }

        ObservableCollection<Item> _sourceCollection;
        public ObservableCollection<Item> SourceCollection
        {
            get => _sourceCollection;
            set => SetProperty(ref _sourceCollection, value);
        }
    }
}