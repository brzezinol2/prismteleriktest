﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace PrismTelerikTest.ViewModels
{
	public class TelerikTreeViewPageViewModel : BindableBase, INavigationAware
	{
        public TelerikTreeViewPageViewModel()
        {
            this.Items = new ItemsViewModel();
        }

        ItemsViewModel _Items;
        public ItemsViewModel Items
        {
            get => _Items;
            set => SetProperty(ref _Items, value);
        }

        private ICommand _buttonCommand;
        public ICommand ButtonCommand { get => _buttonCommand ?? (_buttonCommand = new DelegateCommand<string>(OnButtonCommand)); }

        private void OnButtonCommand(string arg)
        {
            switch (arg)
            {
                case "a":
                    var copyOfItems = new ObservableCollection<Item>(Items.SourceCollection);

                    var toRem = copyOfItems[0].Children.First();

                    copyOfItems[0].Children.Remove(toRem);

                    Items.SourceCollection = copyOfItems;
                    break;
                case "b":
                    var copyOfItems2 = new ObservableCollection<Item>(Items.SourceCollection);

                    copyOfItems2[0].Children.Add(new Item()
                    {
                        Name = "asdasd"
                    });

                    Items.SourceCollection = copyOfItems2;
                    break;
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            try
            {
                //initial
                var item = new Item()
                {
                    Name = "Root",
                    Children = new ObservableCollection<Item>()
                    {
                        new Item()
                        {
                            Name = "Child 1"
                        }
                    }
                };

                //Lance's Telereik Team workaround
                var copyOfItems = new ObservableCollection<Item>(Items.SourceCollection);
                copyOfItems.Add(item);
                Items.SourceCollection = copyOfItems;

                //throws exception
                //Items.SourceCollection.Add(new Item()
                //{
                //    Name = "Added item"
                //});
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
