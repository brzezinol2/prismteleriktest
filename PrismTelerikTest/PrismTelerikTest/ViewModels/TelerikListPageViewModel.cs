﻿using Prism.Navigation;
using System.Collections.ObjectModel;

namespace PrismTelerikTest.ViewModels
{
    public class TelerikListPageViewModel : ViewModelBase
    {
        public TelerikListPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "List Page";

            SampleParameter = "Binding is working.";

            Source = new ObservableCollection<ListItemModel>()
            {
                new ListItemModel(){ Name = "Piotr"},
                new ListItemModel(){ Name = "Natalia"}
            };
        }

        private ObservableCollection<ListItemModel> _source;

        public ObservableCollection<ListItemModel> Source
        {
            get { return this._source; }
            set
            {
                SetProperty(ref _source, value);
            }
        }

        public string SampleParameter { get => sampleParameter; set => SetProperty(ref sampleParameter, value); }

        private string sampleParameter;

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            Source.Clear();
            Source.Add(new ListItemModel() { Name = "Piotr" });
            Source.Add(new ListItemModel() { Name = "Natalia" });
        }
    }

    public class ListItemModel
    {
        public string Name { get; set; }
    }
}
