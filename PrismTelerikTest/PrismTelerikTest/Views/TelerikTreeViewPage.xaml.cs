﻿using Xamarin.Forms;

namespace PrismTelerikTest.Views
{
    public partial class TelerikTreeViewPage : ContentPage
    {
        public TelerikTreeViewPage()
        {
            InitializeComponent();

            tv.HierarchyAdapter = new PrismTelerikTest.ViewModels.TreeViewDemoAdapter();
        }
    }
}
